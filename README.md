# README #


mysqlbackup will backup all mysql-databases on a system into separate files.

## Installation ##

- Download mysqlbackup and save it in /usr/local/bin/
- Change permissions:
  chmod +x /usr/local/bin/mysqlbackup
- Create /var/backups/mysql/
  mkdir /var/backups/mysql/
  
- Add a daily cronjob (in /etc/crontab), in this example the job will run at 23:00 every day.

0 23	* * *	root	/usr/local/bin/backup.sh >> /dev/null 2<&1

Default backups are saved for 7 days, this can be changed in mysqlbackup.
No mysql login information is required. /etc/mysql/debian.cnf is used.


